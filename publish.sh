#!/bin/bash
FUNCTION="my-function-name"
REGION="eu-west-1"
aws lambda update-function-code \
    --zip-file fileb://dist/bundle.zip \
    --region ${REGION} \
    --function-name ${FUNCTION}
